import java.sql.*;
import java.util.ArrayList;

public class AccessDatabase {
	Connection _conn = null;

	public AccessDatabase(String file) {
		// Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		// Class.forName("org.sqlite.JDBC");
		// Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		// _conn = DriverManager.getConnection("jdbc:sqlite:" + file);
		try {
			_conn = DriverManager.getConnection("jdbc:ucanaccess://" + file);
		} catch (Exception e) {
			throw new RuntimeException("cannot open dictionary file", e);
		}
		// _conn = DriverManager.getConnection("jdbc:ucanaccess://" + file);
	}

	public void getWords() {
		Dictionary result = Dictionary.getInstance();

		String sql = "select * from words order by 1";
		try {
			Statement stml = _conn.createStatement();
			ResultSet rs = stml.executeQuery(sql);
			while (rs.next()) {

				Word word = new Word();
				word.no = rs.getInt("no");
				word.spell = rs.getString("word_spell");
				word.pron = rs.getString("pron");
				word.meaning = rs.getString("meaning");

				result.add(word, rs.getInt("level"));
			}
		} catch (Exception e) {
			throw new RuntimeException("cannot get words", e);
		}

	}

	public void saveWordsLevel(int no, int level) {
		try {
			Statement stml = _conn.createStatement();
			String sql = "update words_tbl set level='" + level + "' where no=" + no;
			stml.executeUpdate(sql);
		} catch (SQLException e) {
			throw new RuntimeException("cannot update level", e);
		}

	}

	public void close() {

		if (_conn != null) {
			try {
				_conn.close();
			} catch (Exception e) {
			}
		}
	}

}
