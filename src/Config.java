
public class Config
{
    public static int mainWinW = 1000;
    public static int mainWinH = 50;
    public static int mainWinX = 100;
    public static int mainWinY = 100;
    
    public static int toolTipW = 600;
    public static int toolTipH = 100;
    
    public static float mainWinOpacity = 0.7f;
    public static float OPACITY_STEP = 0.1f;
    public static float OPACITY_MIN = 0.1f;
    
    public static int showWordTime = 2000;
    
    
    public static String DIR_DATA = "data";
    public static String DB_FILE_GRE = DIR_DATA + "/gre.mdb";
    public static String DIR_FONT = "font";
    public static String PRON_FONT_FILE = DIR_FONT + "/KingSoft_Phonetic.ttf";
}
