import java.util.ArrayList;

public class Dictionary {
	
	private static Dictionary _instance = null;

	private ArrayList<Word> _words = new ArrayList<Word>();
	private ArrayList<Integer> _levels = new ArrayList<Integer>();
	private int _knownCount = 0;

	private int _curWordIdx = -1;
	//private Word _curWord = null;
	
    private AccessDatabase _db = null;

	public enum Level {
		KNOWN, UNKOWN
	};

	private Dictionary()
	{
		
	}
	
	public static Dictionary getInstance()
	{
		if(_instance == null)
		{
			_instance = new Dictionary();
		}
		
		return _instance;
	}
	
	public void init()
	{
		  connectDB();
		  _db.getWords();
	}
	
	public void add(Word w, int level) {
		_words.add(w);
		_levels.add(level);
		if (level == Level.KNOWN.ordinal()) {
			_knownCount++;
		}
	}

	public void setWordLevel(boolean known) {
		Level value = known ? Level.KNOWN : Level.UNKOWN;
		_levels.set(_curWordIdx, value.ordinal());
		_db.saveWordsLevel(_words.get(_curWordIdx).no, value.ordinal() );
	}

	public Word next() {
		_curWordIdx++;
		if (_curWordIdx >= _words.size()) {
			_curWordIdx = 0;
		}

		return getCurWord();

	}

	public Word prev() {
		_curWordIdx--;
		if (_curWordIdx < 0) {
			_curWordIdx = _words.size() - 1;
		}

		return getCurWord();

	}

	public Word getCurWord() {
		if (_curWordIdx >= 0 && _curWordIdx < _words.size()) {
			return _words.get(_curWordIdx);
		} else {
			return null;
		}
	}

	public Level getCurLevel() {
		if (_curWordIdx >= 0 && _curWordIdx < _words.size()) {
			return Level.values()[_levels.get(_curWordIdx)];
		} else {
			return null;
		}
	}
	
	public void setCurLevel(boolean known)
	{
		Level value = known ? Level.KNOWN : Level.UNKOWN;
		_levels.set(_curWordIdx, value.ordinal());
		_db.saveWordsLevel(_words.get(_curWordIdx).no, value.ordinal() );
	}
	
	 private void connectDB()
	    {
	        _db = new AccessDatabase(Config.DB_FILE_GRE);
	    }
	 
	 public void close()
	 {
		 _db.close();
	 }

}
