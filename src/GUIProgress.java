import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class GUIProgress extends JDialog {

	private JProgressBar _curProgress = new JProgressBar();
	private JProgressBar _remembered = new JProgressBar();
	
	public GUIProgress(JFrame parent) {
		super(parent, true);

		setSize(300, 50);
		setLocationRelativeTo(null);
		setUndecorated(true);
		
		setFocusTraversalKeysEnabled(false);
		addKeyboardlistener();
		buildPanel();
	}
	
	private void buildPanel()
	{
		JPanel pane = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 0;
		
		pane.add(new JLabel("progress "), c);
		c.gridy = 1;
		pane.add(new JLabel("remembered "), c);
		
		
		c.gridx = 1;
		c.gridy = 0;
		
		_curProgress.setPreferredSize(new Dimension(200, 15));
		pane.add(_curProgress, c);
		c.gridy	 = 1;	
		_remembered.setPreferredSize(new Dimension(200, 15));
		pane.add(_remembered, c);
		
		setLayout(new BorderLayout());
		add(pane, BorderLayout.CENTER);
	}

	private void addKeyboardlistener() {
		addKeyListener(new KeyListener()
        {

            @Override
            public void keyPressed(KeyEvent arg0)
            {
                switch(arg0.getKeyCode())
                {
                case KeyEvent.VK_TAB:
                	
                	GUIProgress.this.setVisible(false);
                	break;
                }
            }

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
        });
	}
}
