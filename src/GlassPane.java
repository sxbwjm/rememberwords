import java.awt.Point;
import java.awt.event.*;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.MouseInputAdapter;

public class GlassPane extends JComponent
{
    MainWin _parent = null;
    Point _lastMouseLoc = null;


    
    public GlassPane(MainWin parent)
    {
        _parent = parent;

        addMouseListener();
        addKeyboardlistener();
        setFocusable(true);
        
        setFocusTraversalKeysEnabled(false);
        
       
    }
    
    private void addMouseListener()
    {
        MouseInputAdapter ls = new MouseInputAdapter()
        {
            @Override
            public void mousePressed(MouseEvent e)
            {
                // left click to drag
                if(e.getButton() != MouseEvent.BUTTON1)
                {
                    return;
                }

                _lastMouseLoc = e.getLocationOnScreen();
               
            }
            
            @Override
            public void mouseDragged(MouseEvent e) {
                
                if(!SwingUtilities.isLeftMouseButton(e))
                {
                    return;
                }
                
                if(_lastMouseLoc == null)
                {
                    _lastMouseLoc = e.getLocationOnScreen();
                }
                else
                {
                    Point cur =  e.getLocationOnScreen();
                    int dx = cur.x - _lastMouseLoc.x;
                    int dy = cur.y - _lastMouseLoc.y;
                    
                    Point loc = _parent.getLocation();
                    _parent.setLocation(loc.x + dx, loc.y + dy);
                    _lastMouseLoc = cur;
                }
                 
            }
        };
        addMouseListener(ls);
        addMouseMotionListener(ls);
    }
    
    private void addKeyboardlistener()
    {
        KeyListener ls = new KeyListener()
        {

            @Override
            public void keyPressed(KeyEvent arg0)
            {
                switch(arg0.getKeyCode())
                {
                    case KeyEvent.VK_CONTROL:
                        _parent.showToolTip();
                        break;
                    case KeyEvent.VK_UP:
                        _parent.increaseOpacity();
                        break;
                    case KeyEvent.VK_DOWN:
                        _parent.decreaseOpacity();
                        break;
                    case KeyEvent.VK_K:
                    case KeyEvent.VK_RIGHT:
                        _parent.showNextWord();
                        break;
                    case KeyEvent.VK_J:
                    case KeyEvent.VK_LEFT:
                        _parent.showPrevWord();
                        break;
                    case KeyEvent.VK_E:
                        _parent.close();
                        break;
                    case KeyEvent.VK_M:
                        _parent.switchStudyMode();
                        break;
                    case KeyEvent.VK_SPACE:
                        _parent.switchSlideMode();
                        break;
                  
                    case KeyEvent.VK_Y:
                        Dictionary.getInstance().setCurLevel(true);
                        _parent.showNextWord();
                        break;
                    case KeyEvent.VK_N:
                    	Dictionary.getInstance().setCurLevel(false);
                        _parent.showNextWord();
                        break;
                        
                    case KeyEvent.VK_TAB:
                    	_parent.showProgress(true);
                    	break;
                        
                }
            }

           

            @Override
            public void keyReleased(KeyEvent arg0)
            {
                switch(arg0.getKeyCode())
                {
                case KeyEvent.VK_CONTROL:
                   _parent.hideToolTip();
                   break;
                case KeyEvent.VK_TAB:
                	_parent.showProgress(false);
                	break;
                }
            }

            @Override
            public void keyTyped(KeyEvent arg0)
            {
               
            }
            
        };
        
        addKeyListener(ls);
    }
    
   
    
   
}
