import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;

public class MainWin extends JFrame
{
    private static final long serialVersionUID = 1L;

    private JLabel            _no              = null;
    private JLabel            _spell           = null;
    private JLabel            _pron            = null;
    private JLabel            _meaning         = null;

    private Dictionary _dictionary = null;
    //private ArrayList<Word>   _words           = null;
    //private int               _curWordIdx      = -1;
    //private Word              _curWord         = null;

    private final int         MAX_LEN_MEANING  = 70;
    
    private Font KNOWN_FONT = null;
    private Font UNKNOWN_FONT = null;

    public enum Mode
    {
        REMEMBER, RECALL
    };
    
//    public enum Level
//    {
//        KNOWN, UNKOWN
//    };

    private Mode _mode = Mode.REMEMBER;
    private Timer _timer = null;
    private boolean _autoSlide = false;
    

    private GUIProgress _guiProgress = null;
    private Progress _progress = null;
    
    public MainWin() throws Exception
    {
        setSize(Config.mainWinW, Config.mainWinH);
        setLocation(Config.mainWinX, Config.mainWinY);
        setUndecorated(true);


        add(buildPanel(), BorderLayout.CENTER);
        GlassPane glass = new GlassPane(this);
        setGlassPane(glass);
        glass.setVisible(true);

        setOpacity(Config.mainWinOpacity);
        setAlwaysOnTop(true);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
        _guiProgress = new GUIProgress(this);

        addWindowFocusListener(new WindowFocusListener()
        {

            @Override
            public void windowGainedFocus(WindowEvent arg0)
            {

            }

            @Override
            public void windowLostFocus(WindowEvent arg0)
            {

            }

        });
        
        _timer = new Timer(Config.showWordTime, new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                showNextWord();
            }
            
        });
        
        _timer.setDelay(Config.showWordTime);
        _dictionary = Dictionary.getInstance();
        _dictionary.init();
        toRememberMode();
        showNextWord();

        _progress = new Progress();

        // we control tooltip show/hide programmatically
        ToolTipManager.sharedInstance().setInitialDelay(0);
        ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);

        setVisible(true);
    }

    public void close()
    {
        _dictionary.close();
        dispose();
        System.exit(0);
    }
    
    public void showProgress(boolean value)
    {
    	_guiProgress.setLocationRelativeTo(this);
    	_guiProgress.setVisible(value);
    }

    public void showToolTip()
    {
        // emulate mouse hover on meaning label to show tooltip
        ToolTipManager.sharedInstance().mouseMoved(
                new MouseEvent(_meaning, 0, 0, 0, 0, 0, 0, false));
    }

    public void hideToolTip()
    {
        // emulate mouse leaving from meaning label to hide tooltip
        ToolTipManager.sharedInstance().mouseMoved(
                new MouseEvent(_no, 0, 0, 0, 0, 0, 0, false));
    }

    public void increaseOpacity()
    {
        Config.mainWinOpacity += Config.OPACITY_STEP;
        if (Config.mainWinOpacity > 1.0f)
        {
            Config.mainWinOpacity = 1.0f;
        }
        setOpacity(Config.mainWinOpacity);
    }

    public void decreaseOpacity()
    {
        Config.mainWinOpacity -= Config.OPACITY_STEP;
        if (Config.mainWinOpacity < Config.OPACITY_MIN)
        {
            Config.mainWinOpacity = Config.OPACITY_MIN;
        }

        setOpacity(Config.mainWinOpacity);
    }

    private JPanel buildPanel()
    {
        JPanel pane = new JPanel();
        FlowLayout layout = new FlowLayout(FlowLayout.LEADING);
        pane.setLayout(layout);

        // pane.setBorder(b);
        _no = new JLabel("");
        _no.setPreferredSize(new Dimension(40, 40));
        _spell = new JLabel("");
        _spell.setPreferredSize(new Dimension(130, 40));
        Font f = _spell.getFont();
        KNOWN_FONT = new Font(f.getName(), Font.PLAIN, f.getSize());
        UNKNOWN_FONT = new  Font(f.getName(), Font.BOLD, f.getSize());
        _pron = new JLabel("");
        _pron.setPreferredSize(new Dimension(140, 40));
        Font font = null;
        try
        {
            //font = Font.createFont(Font.TRUETYPE_FONT, new File(
            //        Config.PRON_FONT_FILE));
            
            font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream(Config.PRON_FONT_FILE));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        _pron.setFont(font.deriveFont(14f));
        _meaning = new JLabel("");

        pane.add(_no);
        pane.add(_spell);
        pane.add(_pron);
        pane.add(_meaning);

        return pane;

    }
    
   

    private String cropMeaningText(String text)
    {
        if (text == null || text.length() < MAX_LEN_MEANING)
        {
            return text;
        }
        else
        {
            return text.replace("\n", " ").substring(0, MAX_LEN_MEANING);
        }

    }

    public void showNextWord()
    {
        showWord(_dictionary.next());
    }

    public void showPrevWord()
    {
        showWord(_dictionary.prev());
    }

    private void showWord(Word w)
    {
        _no.setText("" + w.no);
        _spell.setText(w.spell);
        _pron.setText(w.pron);
        _meaning.setText(cropMeaningText(w.meaning));
        String meaning = w.meaning.replace("\r\n", "<br>");
        meaning = meaning.replace("\r", "<br>");
        meaning = meaning.replace("\n", "<br>");
        _meaning.setToolTipText("<html>" + meaning + "</html>");
        // set up font for spell: known - bold , unknown - plain
        _spell.setFont(_dictionary.getCurLevel() == Dictionary.Level.KNOWN ? KNOWN_FONT : UNKNOWN_FONT);

    }
    

    public void switchStudyMode()
    {
        if (_mode == Mode.REMEMBER)
        {
            toRecallMode();
        }
        else
        {
            toRememberMode();
        }
    }
    
    public void switchSlideMode()
    {
        // do not 
        if(_mode == Mode.RECALL)
        {
            _autoSlide = false;
        }
        else
        {
            _autoSlide = !_autoSlide;
        }
        if(_autoSlide)
        {
            _timer.start();
        }
        else
        {
            _timer.stop();
        }
    }

    private void toRecallMode()
    {
        _mode = Mode.RECALL;
        _meaning.setVisible(false);
        ((JPanel) getContentPane()).setBorder(BorderFactory
                .createBevelBorder(BevelBorder.LOWERED));
    }

    private void toRememberMode()
    {
        _mode = Mode.REMEMBER;
        _meaning.setVisible(true);
        ((JPanel) getContentPane()).setBorder(BorderFactory
                .createBevelBorder(BevelBorder.RAISED));
    }

//    public String getCurMeaning()
//    {
//        return _curWord.meaning;
//    }
  
    

    static public void main(String[] arg)
    {
        // use native look & feel
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e)
        {
        }

        try
        {
            new MainWin();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    
}
