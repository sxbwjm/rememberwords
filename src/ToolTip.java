import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.*;


public class ToolTip extends JDialog
{
    JTextArea _text = null;
    public ToolTip()
    {
        this.setUndecorated(true);
        this.setOpacity(0.8f);
        _text = new JTextArea();
        _text.setEditable(false);
        _text.setBackground(Color.WHITE);
        
        this.setSize(Config.toolTipW, Config.toolTipH);
        this.add(_text, BorderLayout.CENTER);
    }
    
    public void setText(String text)
    {
        _text.setText(text);
    }
    
}
